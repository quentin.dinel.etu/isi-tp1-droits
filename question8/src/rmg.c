#include <libgen.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "check_pass.h"

/**
 * @brief Affiche un message d'erreur sur la sortie d'erreur, ferme le fichier et arrête le programme.
 * 
 * @param msg Le message d'erreur à afficher
 * @param file Le fichier à fermer
 */
void print_error(char *msg, FILE *file)
{
    fprintf(stderr, "%s\n", msg);
    if (file != NULL) fclose(file);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
    assert(argc == 2);
    char *filename = argv[1];

    if (access(filename, F_OK) != 0)
        print_error("Le fichier n'existe pas.", NULL);
    
    FILE *file = fopen(filename, "r");

    /* Vérifie que l'utilisateur possède le même groupe que le fichier à supprimer */
    if (!check_group(filename))
        print_error("L'utilisateur n'appartient pas au même groupe que le fichier.", file);

    /* Demande son mot de passe à l'utilisateur */
    char *password = getpass("Mot de passe : ");

    /* Vérifie dans le fichier passwd que l'utilisateur est bien associé à ce mot de passe */
    if (!check_pass(crypt(password, "42")))
        print_error("L'utilisateur et le mot de passe ne correspondent pas.", file);

    /* Efface le fichier */
    if (remove(filename) == 0)
        printf("Le fichier %s a été supprimé avec succès.\n", basename(filename));
    else
        print_error("Impossible de supprimer le fichier.", file);

    fclose(file);
    return 0;
}