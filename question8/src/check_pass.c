#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "check_pass.h"

/**
 * @brief Vérifie que l'utilisateur possède le même groupe que le fichier à supprimer.
 * 
 * @param file Le chemin vers le fichier
 * @return 1 si l'utilisateur possède le même groupe que le fichier, 0 sinon
 */
int check_group(char *file)
{
    struct stat st;
    stat(file, &st);

    return st.st_gid == getgid();
}

/**
 * @brief Vérifie dans le fichier passwd que l'utilisateur est bien associé à ce mot de passe.
 * 
 * @param password Le mot de passe de l'utilisateur
 * @return 1 si l'utilisateur et le mot de passe correspondent, 0 sinon
 */
int check_pass(char *password)
{
    FILE *passwd = fopen("/home/admin/passwd", "r");

    int bufferLength = 255;
    char buffer[bufferLength];
    while(fgets(buffer, bufferLength, passwd))
    {
        char *usr = strtok(buffer, ";");
        if (strcmp(usr, getenv("USERNAME")) == 0) 
        {
            char *psw = strtok(NULL, "\n");
            if (strcmp(psw, password) == 0)
            {
                fclose(passwd);
                return 1;
            }
        }
    }

    fclose(passwd);
    return 0;
}
