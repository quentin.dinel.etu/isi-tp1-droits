#!/bin/bash

cd /home/server

#peuvent lire tous les fichiers et sous-répertoires contenus dans dir_a
cd ./dir_a 
if [[ $? -eq 0 ]]
then
    echo "OK : L'utilisateur lambda_a a accès au dossier dir_a."
else
    echo "ERREUR : L'utilisateur lambda_a n'a pas accès au dossier dir_a."
fi
cd /home/server

#peuvent lire le contenu des fichiers dans dir_a
cat ./dir_a/a.txt >/dev/null
if [[ $? -eq 0 ]]
then
    echo "OK : L'utilisateur lambda_a peut lire le contenu des fichiers du dossier dir_a."
else
    echo "ERREUR : L'utilisateur lambda_a ne peut pas lire le contenu des fichiers du dossier dir_a."
fi

#peuvent modifier des fichiers dans dir_a
echo 'Je modifie a.txt !'>./dir_a/a.txt
if [[ $? -eq 0 ]]
then
    echo "OK : L'utilisateur lambda_a peut modifier le contenu des fichiers du dossier dir_a."
else
    echo "ERREUR : L'utilisateur lambda_a ne peut pas modifier le contenu des fichiers du dossier dir_a."
fi

#peuvent créer des fichiers dans dir_a
touch ./dir_a/a3.txt 
if [[ $? -eq 0 ]]
then
    echo "OK : L'utilisateur lambda_a peut créer de nouveaux fichiers dans le dossier dir_a."
else
    echo "ERREUR : L'utilisateur lambda_a ne peut pas créer de nouveaux fichiers dans le dossier dir_a."
fi

#ne peuvent pas lire tous les fichiers et sous-répertoires contenus dans dir_b
cd ./dir_b 
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a n'a pas accès au dossier dir_b."
else
    echo "ERREUR : L'utilisateur lambda_a a accès au dossier dir_b."
fi
cd /home/server

#peuvent lire le contenu des fichiers dans dir_b
cat ./dir_b/b.txt >/dev/null
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas lire le contenu des fichiers du dossier dir_b."
else
    echo "ERREUR : L'utilisateur lambda_a peut lire le contenu des fichiers du dossier dir_b."
fi

#ne peuvent pas modifier des fichiers dans dir_b
echo 'Je modifie b.txt !'>./dir_b/b.txt 
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas modifier le contenu des fichiers du dossier dir_b."
else
    echo "ERREUR : L'utilisateur lambda_a peut modifier le contenu des fichiers du dossier dir_b."
fi

#ne peuvent pas renommer des fichiers dans dir_b
mv ./dir_b/b.txt ./dir_b/a4.txt 
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas renommer le contenu des fichiers du dossier dir_b."
else
    echo "ERREUR : L'utilisateur lambda_a peut renommer le contenu des fichiers du dossier dir_b."
fi

#ne peuvent pas effacer des fichiers dans dir_b
rm -f ./dir_b/b.txt
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas effacer le contenu des fichiers du dossier dir_b."
else
    echo "ERREUR : L'utilisateur lambda_a peut effacer le contenu des fichiers du dossier dir_b."
fi

#ne peuvent pas créer des fichiers dans dir_b
touch ./dir_b/a5.txt
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas créer de nouveaux fichiers dans le dossier dir_b."
else
    echo "ERREUR : L'utilisateur lambda_a peut créer de nouveaux fichiers dans le dossier dir_b."
fi

#peuvent lire tous les fichiers et sous-répertoires contenus dans dir_c
cd ./dir_c
if [[ $? -eq 0 ]]
then
    echo "OK : L'utilisateur lambda_a a accès au dossier dir_c."
else
    echo "ERREUR : L'utilisateur lambda_a n'a pas accès au dossier dir_c."
fi
cd /home/server

#peuvent lire le contenu des fichiers dans dir_c
cat ./dir_c/c.txt >/dev/null
if [[ $? -eq 0 ]]
then
    echo "OK : L'utilisateur lambda_a peut lire le contenu des fichiers du dossier dir_c."
else
    echo "ERREUR : L'utilisateur lambda_a ne peut pas lire le contenu des fichiers du dossier dir_c."
fi

#ne peuvent pas modifier des fichiers dir_c
echo "Je modifie c.txt !" > ./dir_c/c.txt 
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas modifier le contenu des fichiers du dossier dir_c."
else
    echo "ERREUR : L'utilisateur lambda_a peut modifier le contenu des fichiers du dossier dir_c."
fi

#ne peuvent pas renommer des fichiers dir_c
mv ./dir_c/c.txt ./dir_c/c2.txt 
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas renommer le contenu des fichiers du dossier dir_c."
else
    echo "ERREUR : L'utilisateur lambda_a peut renommer le contenu des fichiers du dossier dir_c."
fi

#ne peuvent pas effacer des fichiers dir_c
rm -f ./dir_c/c.txt 
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas effacer le contenu des fichiers du dossier dir_c."
else
    echo "ERREUR : L'utilisateur lambda_a peut effacer le contenu des fichiers du dossier dir_c."
fi

#ne peuvent pas creer des fichiers dir_c
touch ./dir_c/c3.txt 
if [[ $? -ne 0 ]]
then
    echo "OK : L'utilisateur lambda_a ne peut pas créer de nouveaux fichiers dans le dossier dir_c."
else
    echo "ERREUR : L'utilisateur lambda_a peut créer de nouveaux fichiers dans le dossier dir_c."
fi

#Delete fichiers créés durant le test de vérifications et créations des fichiers de bases a.txt, b.txt, c.txt
rm -f ./dir_a/*
rm -f ./dir_b/*
rm -f ./dir_c/*
touch ./dir_a/a.txt
touch ./dir_b/b.txt
touch ./dir_c/c.txt