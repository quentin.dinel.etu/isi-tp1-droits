# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- DINEL, Quentin, quentin.dinel.etu@univ-lille.fr

- GARCIA, Thibault, thibault.garcia.etu@univ-lille.fr

## Question 1

```bash
sudo adduser -g centos toto
```

L'utilisateur toto peut écrire car il fait partie du groupe ubuntu qui possède les droits d'écriture sur le fichier (-r--r<u>w</u>--r--).

## Question 2

Cela signifie que l'on peut accéder aux fichiers et aux dossiers dans ce répertoire. On peut aussi y créer de nouveaux fichiers et dossiers.

### Créer un dossier mydir et retirer les droits d'exécution d'un dossier au groupe associé au dossier, avec l'utilisateur centos :

```bash
mkdir mydir
chmod g-x mydir
```

### Tenter d'accéder au dossier, avec l'utilisateur toto :

```bash
cd ../centos/mydir
```

OUTPUT:

    -bash: cd: ../centos/mydir: Permission non accordée

Faisant partie du groupe centos et ayant retiré les droits d'exécution au dossier mydir, toto ne peut pas se déplacer dedans pour accèder aux autres fichiers ou en créer de nouveaux.

### Création du fichier data.txt par centos et tentative de visionnage des droits par toto :

```bash
touch mydir/data.txt
```

OUTPUT:

    ls -al ../centos/mydir/
    ls: impossible d'accéder à mydir/.: Permission non accordée
    ls: impossible d'accéder à mydir/..: Permission non accordée
    ls: impossible d'accéder à mydir/data.txt: Permission non accordée
    total 0
    d????????? ? ? ? ?              ? .
    d????????? ? ? ? ?              ? ..
    -????????? ? ? ? ?              ? data.txt

Bien que nous puissons lister les fichiers présents dans le répertoire mydir, nous n'avons accès à aucune informations sur ces derniers quand nous lançons la commande à partir de l'utilisateur toto.

## Question 3

### Exécution du fichier suid par l'utilisateur centos (=ubuntu) :

```bash
[centos@isi ~]$ ./suid mydir/data.txt
EUID : 1000
EGID : 1000
RUID : 1000
RGID : 1000
```

### Exécution du fichier suid par l'utilisateur toto :

    [centos@isi ~]$ sudo runuser -l toto -c '../centos/suid ../centos/mydir/data.txt'
    EUID : 1001
    EGID : 1000
    RUID : 1001
    RGID : 1000
    Cannot open file: Permission denied

L'EUID ainsi que le RUID vaut 1001 ce qui correspond à l'utilisateur toto. Les autres sont ceux du groupe centos.  
Toto n'arrive pas à afficher le contenu du fichier.

### Activation du flag set-user-id :

```bash
[centos@isi ~]$ chmod u+s suid
```

### Réexécution du fichier suid par l'utilisateur toto :

    [toto@isi ~]$ ../centos/suid '../centos/mydir/data.txt'
    EUID : 1000
    EGID : 1000
    RUID : 1001
    RGID : 1000
    Hello world

Cette fois, toto arrive à lire le contenu du fichier data.txt.

## Question 4

```bash
[centos@isi ~]$ chmod u+s suid
```

```bash
[toto@isi centos]$ python3 mydir/suid.py
```

//Permission non accordée pour l'instant
q

## Question 5

chfn est une commande pour renseigner des informations supplémentaires de l'utilisateur : Nom, Adresse de Bureau, Téléphone bureau, Téléphone domicile.

    [centos@isi ~]$ ls -al /usr/bin/chfn
    -rws--x--x. 1 root root 24048 16 août   2018 /usr/bin/chfn

Toto a pu exécuter la commande chfn car il en avait les permissions comme le montre le 4ième caractère "s".

Nous avons ensuite renseignés des informations en faisant un chfn chez toto et un cat /etc/passwd nous affiche ceci :

    toto:x:1001:1000:t0t0leb0ss,a,b,c:/home/toto:/bin/bash

Ce qui prouve que le changement d'informations à bien était pris en compte.

## Question 6

Les mots de passes sont stockés dans le fichier etc/shadow. La raison est que tout le monde a accès à etc/passwd donc il fallait créer un autre fichier seulement accessible par le super utilisateur.

## Question 7

Ordre d'exécution des commandes bash pour la création de l'architecture voulue :

```bash
#Création de l'admin
sudo adduser admin
#Création des groupes
sudo groupadd groupe_a
sudo groupadd groupe_b
#Création des utilisateurs
sudo adduser lambda_a
sudo adduser lambda_b
#Affectation aux groupes
sudo usermod -aG groupe_a lambda_a
sudo usermod -aG groupe_b lambda_b
#Vérification
groups lambda_a
groups lambda_b
groups admin
#Création des répertoires
#On créé ici un répertoire serveur dans le dossier home pour avoir un lieu neutre appertenant à root.
sudo mkdir /home/server
cd /home/server
sudo mkdir dir_a
sudo mkdir dir_b
sudo mkdir dir_c
#Création des fichiers dans les répertoires
sudo touch dir_a/a.txt
sudo touch dir_b/b.txt
sudo touch dir_c/c.txt
#On modifie les ownership et groupes des répertoires
sudo chown admin dir_a
sudo chown admin dir_b
sudo chown admin dir_c
sudo chgrp groupe_a dir_a
sudo chgrp groupe_b dir_b
sudo chgrp admin dir_c
#On modifie les ownership et groupes des fichiers
sudo chown admin a.txt
sudo chown admin b.txt
sudo chown admin c.txt
sudo chgrp groupe_a a.txt
sudo chgrp groupe_b b.txt
sudo chgrp admin c.txt
```

Chmod sur les répertoires et fichiers :

```bash
#Permissions dir_c
##Lecture à tous le monde
sudo chmod ugo+r dir_c

ls -la
drwxr-xr-x  5 root  root     45 19 janv. 16:42 .
drwxr-xr-x. 8 root  root     91 19 janv. 16:27 ..
drwxr-xr-x  2 admin groupe_a 19 19 janv. 16:45 dir_a
drwxr-xr-x  2 admin groupe_b 19 19 janv. 16:46 dir_b
drwxr-xr-x  2 admin admin    19 19 janv. 16:47 dir_c

##Exécution qu'au owner et other
sudo chmod g-x dir_c
##Ecriture au groupe
sudo chmod g+w dir_c

#Permissions dir_b
##Ecriture au groupe (+ owner déjà présent)
sudo chmod g+w dir_b
##Exécution qu'au owner
sudo chmod o-x dir_b
#Owner n'a pas le droit de write
sudo chmod u-w dir_b

#Permissions dir_a
##Ecriture au groupe (+ owner déjà présent)
sudo chmod g+w dir_a
##Exécution qu'au owner et au groupe
sudo chmod o-x dir_a
#Owner n'a pas le droit de write
sudo chmod u-w dir_a

#L'état final des permissions des répertoires :
ls -la
drwxr-xr-x  5 root  root     45 19 janv. 16:42 .
drwxr-xr-x. 8 root  root     91 19 janv. 16:27 ..
dr-xrw-r--  2 admin groupe_a 19 19 janv. 16:45 dir_a
dr-xrw-r--  2 admin groupe_b 19 19 janv. 16:46 dir_b
drwxr--r--  2 admin admin    19 19 janv. 16:47 dir_c
```
