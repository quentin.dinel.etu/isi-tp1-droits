#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
    FILE *f;
    
    if (argc < 2)
    {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }
    
    printf("EUID : %d\n", geteuid());
    printf("EGID : %d\n", getegid());
    printf("RUID : %d\n", getuid());
    printf("RGID : %d\n", getgid());

    f = fopen(argv[1], "r");
    if (f == NULL)
    {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    
    char c = fgetc(f); 
    while (c != EOF) 
    { 
        printf ("%c", c); 
        c = fgetc(f); 
    } 

    fclose(f);
    exit(EXIT_SUCCESS);
}
