#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "check_pass_2.h"

int main(void)
{
    int exists = 0;

    /* Vérifie dans passwd si l'utilisateur a déjà un mot de passe */
    if (check_if_pass_exists())
    {
        exists = 1;

        /* Si oui, demande l'ancien mot de passe */
        char *password = getpass("Mot de passe : ");

        /* Vérifie dans passwd si l'ancien mot de passe est correct */
        if (!check_pass(crypt(password, "42")))
        {
            fprintf(stderr, "Le mot de passe est incorrect.\n");
            exit(EXIT_FAILURE);
        }
    }

    /* Demande le nouveau mot de passe */
    char *password = getpass("Tapez votre nouveau mot de passe : ");

    /* Crypte le mot de passe */
    char *crpyted_password = crypt(password, "42");

    /* Supprime l'ancien mot de passe du passwd s'il existe */
    if (exists)
        remove_pass();

    /* Ajoute le nouveau mot de passe crpyté au passwd */
    add_pass(crpyted_password);

    return 0;
}