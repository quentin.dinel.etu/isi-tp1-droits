#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "check_pass_2.h"

/**
 * @brief Vérifie dans passwd si l'utilisateur a déjà un mot de passe
 * 
 * @return 1 si l'utilisateur a déjà un mot de passe, 0 sinon
 */
int check_if_pass_exists()
{
    FILE *passwd = fopen("/home/admin/passwd", "r");

    int bufferLength = 255;
    char buffer[bufferLength];
    while(fgets(buffer, bufferLength, passwd))
    {
        char *usr = strtok(buffer, ";");
        if (strcmp(usr, getenv("USERNAME")) == 0)
        {
            fclose(passwd);
            return 1;
        }
    }
    fclose(passwd);
    return 0;
}

/**
 * @brief Vérifie dans le fichier passwd que l'utilisateur est bien associé à ce mot de passe.
 * 
 * @param password Le mot de passe de l'utilisateur
 * @return 1 si l'utilisateur et le mot de passe correspondent, 0 sinon
 */
int check_pass(char *password)
{
    FILE *passwd = fopen("/home/admin/passwd", "r");

    int bufferLength = 255;
    char buffer[bufferLength];
    while(fgets(buffer, bufferLength, passwd))
    {
        char *usr = strtok(buffer, ";");
        if (strcmp(usr, getenv("USERNAME")) == 0) 
        {
            char *psw = strtok(NULL, "\n");
            if (strcmp(psw, password) == 0)
            {
                fclose(passwd);
                return 1;
            }
        }
    }

    fclose(passwd);
    return 0;
}

/**
 * @brief Supprime le mot de passe de l'utilisateur
 * 
 */
void remove_pass()
{
    FILE *passwd = fopen("/home/admin/passwd", "rw");
    int cpt = 1;
    
    int bufferLength = 255;
    char buffer[bufferLength];
    while(fgets(buffer, bufferLength, passwd))
    {
        char *usr = strtok(buffer, ";");
        if (strcmp(usr, getenv("USERNAME")) == 0) 
        {
            char buf[32];
            sprintf(buf,"sed -i -e '%d,%dd' passwd", cpt, cpt);
            system(buf);
        }
        cpt++;
    }

    fclose(passwd);
}

/**
 * @brief Ajoute un mot de passe à l'utilisateur
 * 
 * @param password Le mot de passe à ajouter
 */
void add_pass(char *password)
{
    FILE *passwd = fopen("/home/admin/passwd", "a");
    fprintf(passwd, "%s;%s", getenv("USERNAME"), password);
    fclose(passwd);
}