#ifndef __CHECK_PASS_2__
#define __CHECK_PASS_2__

int check_if_pass_exists();
int check_pass(char *password);
void remove_pass();
void add_pass(char *password);

#endif